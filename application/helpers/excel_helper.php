<?php


	/**
	* get the column name (A,B,C,...)
	* @param string|int $num 	An associative string of initialization num
	* @return string $letter
	*/
	function get_excel_column_name_by_row_index($num = '')
	{
	    $numeric = $num % 26;
	    $letter = chr(65 + $numeric);
	    $num2 = intval($num / 26);

	    if ($num2 > 0)
	    {
	       return get_excel_column_name_by_row_index($num2 - 1) . $letter;
	    }

	    return $letter;
	}

	/**
	* getting all the columns that has data on the excel
	* @param string $end_column			An associative string of initialization end_column
	* @param string $first_letters		An associative string of initialization first_letters
	* @return array $columns
	*/
	function create_columns_array($end_column ='', $first_letters = '')
	 {
		 $columns = array();
		 $length = strlen($end_column);
		 $letters = range('A', 'Z');

		 // Iterate over 26 letters.
		 foreach ($letters as $letter)
		 {
			 // Paste the $first_letters before the next.
			 $column = $first_letters . $letter;

			 // Add the column to the final array.
			 $columns[] = $column;

			 // If it was the end column that was added, return the columns.
			 if ($column == $end_column)
			 return $columns;
		 }

		 // Add the column children.
		 foreach ($columns as $column)
		 {
			 // Don't itterate if the $end_column was already set in a previous itteration.
			 // Stop iterating if you've reached the maximum character length.
			 if (!in_array($end_column, $columns) && strlen($column) < $length)
			 {
				 $new_columns = create_columns_array($end_column, $column);
				 // Merge the new columns which were created with the final columns array.
				 $columns = array_merge($columns, $new_columns);
			 }
		 }

		 return $columns;
	 }

	/**
	* generates an excel file
	* in using this helper, the param $data should be a two dimensional array, E.g.
	* $data['array_name_1'] => array (
	*	['array_name 2'] => array (
	*			['data_1'] => '',
	*			['data_2'] => ''
	*		)
	*	)
	* styles on excel is already inside the function, just add bold, underline, upperline, etc. on the key of the array_name_2 in example above. E.g. ['bold_underline_array_name_2']
	* there are special function below for specific reports for manipulation of rows and columns or other styles.
	* You can add more styles and special functions if you want to, read comments below for more instruction
	* E.g of $tmp_folder = FCPATH . 'tmp/csv_download/' . $data['company_info']['slug'] . '/';  		->this is where the data will be saved
	* E.g of $filename = "Batch_Deduction_Report" . strtotime(date("y-m-d H:i:s")) . ".xls";			->this is the name of the file
	* E.g of $file = base_url() . 'tmp/csv_download/' . $data['company_info']['slug'] . '/' .$filename;	->this will be the data that should be open once saving is done
	*
	* @param array 	$data 			An associative array of initialization data
	* @param string $tmp_folder 	An associative string of initialization tmp_folder
	* @param string $filename 		An associative string of initialization filename
	* @param string $file 			An associative string of initialization file
	* @return array $return
	*/
	function excel_generate($data = array(), $tmp_folder ='', $filename ='', $file ='')
	{

		$CI =& get_instance();

		$CI->load->library('excel');

		//information of the excel
		$CI->excel->setActiveSheetIndex(0);
		$CI->excel->getProperties() ->setCreator("AGUILA POS")
									->setLastModifiedBy("AGUILA POS")
									->setTitle(empty($data['report_type'][0]) ? '(None)' : $data['report_type'][0])
									->setSubject(empty($data['report_type'][0]) ? '(None)' : $data['report_type'][0]); //the parameter of data should have atleast $data['report_type'] with value inorder to have title and subject of the excel

		$CI->excel->getActiveSheet()->setTitle(empty($data['report_type'][0]) ? '(None)' : $data['report_type'][0]);

		$counter_merge_top	= 0; //for merging top for the title

		//get data
		if(array_check($data))
		{
			$x 					= 1; //for the rows
			$counter_merge_top	= 0; //for merging top for the title
			$counter_merge_top1	= 0; //for merging top for the title

			foreach ($data as $key => $value)
			{

				$retain_x = 1;	//if value becomes 0, the row will not move

				//finding values on the key of the 2 dimensional array, if not boolean then configure the design of the specific cell

				//text fonts and style and format
				$find_bold							= strpos($key, 'bold');
				$find_underline						= strpos($key, 'underline');
				$find_upperline						= strpos($key, 'upperline');
				$find_double_underline				= strpos($key, 'doubledline');
				$find_double_upperline				= strpos($key, 'doubleuline');
				$find_center						= strpos($key, 'center');
				$find_left_align					= strpos($key, 'left_align');
				$find_right_align					= strpos($key, 'right_align');
				$find_justify						= strpos($key, 'justify');
				$find_money							= strpos($key, 'money'); //change format of the money
				$find_not_first						= strpos($key, 'not_first');

				//merging columns (you can add more depending ion how many columns you are need to merge)
				$find_merge_2 						= strpos($key, 'merge2'); //merge two(2) columns
				$find_merge_4						= strpos($key, 'merge4'); //merge two(2) columns
				$find_merge_top						= strpos($key, '#$%#!'); //considered as special function that the 3 rows above will be at the center
				$find_merge_top2					= strpos($key, 'mergetop2'); //considered as special function that the 3 rows above will be at the center
				$find_merge_top1					= strpos($key, 'mergetop1'); //considered as special function that the 5 rows above will be at the center

				//plus the numner of $k then minus to the number coresponds to the key
				$find_plus_one	 					= strpos($key, 'plus_one');
				$find_plus_one_only	 				= strpos($key, 'plus_one_only');
				$find_plus_two	 					= strpos($key, 'plus_two');
				$find_plus_two_only	 				= strpos($key, 'plus_two_only');
				$find_plus_three		 			= strpos($key, 'plus_three'); //with retain x
				$find_plus_three_only				= strpos($key, 'plus_three_only'); //does not retain x
				$find_plus_four_only 				= strpos($key, 'plus_four_only'); //does not retain x
				$find_plus_four 					= strpos($key, 'plus_four');
				$find_plus_five	 					= strpos($key, 'plus_five');
				$find_plus_six	 					= strpos($key, 'plus_six');
				$find_plus_six_only	 				= strpos($key, 'plus_six_only');
				$find_plus_nineteen	 				= strpos($key, 'plus_nineteen');
				$find_plus_twenty 					= strpos($key, 'plus_twenty');

				$find_data_only 					= strpos($key, 'data_only');

				if(array_check($value))
				{
					foreach ($value as $k => $v)
					{

						if($find_data_only){
							$CI->excel->getActiveSheet()->setCellValue(get_excel_column_name_by_row_index($k).$x, $v);
							continue;
						}
						//if the key finds a bold, the text will be bold on the excel
						if(!is_bool($find_bold))
						{
							$CI->excel->getActiveSheet()->getStyle(get_excel_column_name_by_row_index($k).$x)->getFont()->setBold(true);
						}

						//if the key finds a underline, the text will have an underline on the excel
						if(!is_bool($find_underline))
						{
							$CI->excel->getActiveSheet()->getStyle(get_excel_column_name_by_row_index($k).$x)->getBorders()->getBottom()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
						}

						//if the key finds a upperline, the text will have an upperline on the excel
						if(!is_bool($find_upperline))
						{
							$CI->excel->getActiveSheet()->getStyle(get_excel_column_name_by_row_index($k).$x)->getBorders()->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
						}

						//if the key finds a doubledline, the text will have double underline on the excel
						if(!is_bool($find_double_underline))
						{
							$CI->excel->getActiveSheet()->getStyle(get_excel_column_name_by_row_index($k).$x)->getBorders()->getBottom()->setBorderStyle(PHPExcel_Style_Border::BORDER_DOUBLE);
						}

						//if the key finds a doubleuline, the text will have double upperline on the excel
						if(!is_bool($find_double_upperline))
						{
							$CI->excel->getActiveSheet()->getStyle(get_excel_column_name_by_row_index($k).$x)->getBorders()->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_DOUBLE);
						}

						//if the key finds a center, the text will be aligned in center
						if(!is_bool($find_center))
						{
							$CI->excel->getActiveSheet()->getStyle(get_excel_column_name_by_row_index($k).$x)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
						}

						//if the key finds a left_align, the text will be aligned in left
						if(!is_bool($find_left_align))
						{
							$CI->excel->getActiveSheet()->getStyle(get_excel_column_name_by_row_index($k).$x)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
						}

						//if the key finds a right_align, the text will be aligned in right
						if(!is_bool($find_right_align))
						{
							$CI->excel->getActiveSheet()->getStyle(get_excel_column_name_by_row_index($k).$x)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
						}

						//if the key finds a justify, the text will be aligned justify
						if(!is_bool($find_justify))
						{
							$CI->excel->getActiveSheet()->getStyle(get_excel_column_name_by_row_index($k).$x)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_JUSTIFY);
						}

						//if the key finds a doubleuline, the text will have double upperline on the excel
						if(!is_bool($find_money))
						{
							if(!is_bool($find_not_first) )
							{
								$CI->excel->getActiveSheet()->getStyle(get_excel_column_name_by_row_index($k+19).($x))->getNumberFormat()->setFormatCode('###,###,##0.00;(###,###,##0.00)');
							}elseif(is_bool($find_not_first)){
								$CI->excel->getActiveSheet()->getStyle(get_excel_column_name_by_row_index($k+19).($x))->getNumberFormat()->setFormatCode('###,###,##0.00;(###,###,##0.00)');
							}
						}

						//merge 2 columns into one
						if (!is_bool($find_merge_2))
						{
							$CI->excel->setActiveSheetIndex(0)->mergeCells(get_excel_column_name_by_row_index($k).$x .':' .get_excel_column_name_by_row_index($k+1).$x );
							$CI->excel->getActiveSheet()->getStyle(get_excel_column_name_by_row_index($k).$x .':' .get_excel_column_name_by_row_index($k+1).$x )->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
						}

						//merge 4 columns into one
						if (!is_bool($find_merge_4))
						{
							$CI->excel->setActiveSheetIndex(0)->mergeCells(get_excel_column_name_by_row_index($k).$x .':' .get_excel_column_name_by_row_index($k+3).$x );
							$CI->excel->getActiveSheet()->getStyle(get_excel_column_name_by_row_index($k).$x .':' .get_excel_column_name_by_row_index($k+1).$x )->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
						}



						//merge top
						if (!is_bool($find_merge_top))
						{
							$counter_merge_top = 1;
						}

						//merge top
						if (!is_bool($find_merge_top2))
						{
							$counter_merge_top = 1;
						}

						//merge top1
						if (!is_bool($find_merge_top1))
						{
							$counter_merge_top1 = 1;
						}


						//for data to be put in the excel
						if (!is_bool($find_plus_one))
						{
							if(is_bool($find_plus_one_only))
							{
								$retain_x = 0;
							}

							$CI->excel->getActiveSheet()->setCellValue(get_excel_column_name_by_row_index($k+1).($x-1), $v);
						}
						else if (!is_bool($find_plus_two))
						{
							if(is_bool($find_plus_two_only))
							{
								$retain_x = 0;
							}
							$CI->excel->getActiveSheet()->setCellValue(get_excel_column_name_by_row_index($k+2).($x-1), $v);
						}
						else if (!is_bool($find_plus_three))
						{
							if(is_bool($find_plus_three_only))
							{
								$retain_x = 0;
							}
							$CI->excel->getActiveSheet()->setCellValue(get_excel_column_name_by_row_index($k+3).($x-1), $v);
						}

						else if (!is_bool($find_plus_four))
						{
							if(is_bool($find_plus_four_only))
							{
								$retain_x = 0;
							}
							$CI->excel->getActiveSheet()->setCellValue(get_excel_column_name_by_row_index($k+4).($x-1), $v);
						}
						else if (!is_bool($find_plus_five))
						{
							$retain_x = 0;
							$CI->excel->getActiveSheet()->setCellValue(get_excel_column_name_by_row_index($k+5).($x-1), $v);
						}
						else if (!is_bool($find_plus_six))
						{
							if(is_bool($find_plus_six_only))
							{
								$retain_x = 0;
							}
							$CI->excel->getActiveSheet()->setCellValue(get_excel_column_name_by_row_index($k+6).($x-1), $v);
						}

						else if (!is_bool($find_plus_nineteen))
						{
							$retain_x = 0;
							$CI->excel->getActiveSheet()->setCellValue(get_excel_column_name_by_row_index($k+19).($x-1), $v);
						}
						else if (!is_bool($find_plus_twenty))
						{
							$retain_x = 0;
							$CI->excel->getActiveSheet()->setCellValue(get_excel_column_name_by_row_index($k+20).($x-1), $v);
						}

						else
						{
							$CI->excel->getActiveSheet()->setCellValue(get_excel_column_name_by_row_index($k).$x, $v);
						}
					}
				}

				if($retain_x == 1)
				{
					$x++;
				}
			}
		}
		//getting the highest column render on the excel
		$highest_column = $CI->excel->setActiveSheetIndex(0)->getHighestColumn();

		//merging top rows
		if($counter_merge_top == 1)
		{
			//first row, usually company name
			$CI->excel->setActiveSheetIndex(0)->mergeCells('A1:' .$highest_column.'1');
			$CI->excel->getActiveSheet()->getStyle('A1:' .$highest_column.'1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

			//second row, usually report type
			$CI->excel->setActiveSheetIndex(0)->mergeCells('A2:' .$highest_column.'2');
			$CI->excel->getActiveSheet()->getStyle('A2:' .$highest_column.'2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

			//third row, usually date or period covered
			$CI->excel->setActiveSheetIndex(0)->mergeCells('A3:' .$highest_column.'3');
			$CI->excel->getActiveSheet()->getStyle('A3:' .$highest_column.'3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

		}

        /*
		//special function : merging top rows for Time and Attendance DTR
		if($counter_merge_top1 == 1)
		{
			//first row, usually company name
			$CI->excel->setActiveSheetIndex(0)->mergeCells('A1:' .$highest_column.'1');
			$CI->excel->getActiveSheet()->getStyle('A1:' .$highest_column.'1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

			//second row, usually report type
			$CI->excel->setActiveSheetIndex(0)->mergeCells('A2:' .$highest_column.'2');
			$CI->excel->getActiveSheet()->getStyle('A2:' .$highest_column.'2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

			//third row, usually date or period covered
			$CI->excel->setActiveSheetIndex(0)->mergeCells('A3:' .$highest_column.'3');
			$CI->excel->getActiveSheet()->getStyle('A3:' .$highest_column.'3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

			//fourth row, date created
			$CI->excel->setActiveSheetIndex(0)->mergeCells('A4:' .$highest_column.'4');
			$CI->excel->getActiveSheet()->getStyle('A4:' .$highest_column.'4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

			//fifth row, creator
			$CI->excel->setActiveSheetIndex(0)->mergeCells('A5:' .$highest_column.'5');
			$CI->excel->getActiveSheet()->getStyle('A5:' .$highest_column.'5')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

		}
        */

		//autosizing the cell of the excel
		$var = create_columns_array($highest_column);

		foreach($var as $columnID)
		{
			$CI->excel->getActiveSheet()->getColumnDimension($columnID)->setAutoSize(true);
		}

		//make a new directory if the $tmp_folder is not yet existing
		if(!file_exists($tmp_folder)){
			mkdir($tmp_folder,0777,true);
		}

		$tmp_folder = $tmp_folder.$filename;

		//write the excel on the directory
		$objWriter = PHPExcel_IOFactory::createWriter($CI->excel, 'Excel5');
		$objWriter->save($tmp_folder);


		$return = array('url' => $file,'file'=>$filename);

		foreach($CI->excel->getActiveSheet()->getRowDimensions() as $rd) { 
		    $rd->setRowHeight(-1); 
		}

		//return $return
		return $return;
	}

?>
