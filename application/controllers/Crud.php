<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Crud extends CI_Controller {

	/****************************************/
	/****************************************/
	/*			 DEVELOPED BY:	 			*/
	/*			 KENN JEUS SAGUN 			*/
	/****************************************/
	/****************************************/

	function __construct()
    {
        parent::__construct(); 
        $this->load->model('Crud_model');
        $this->load->helper('array_helper');
    }

	public function index()
	{
		$header['nav'] 			= 'CRUD';
		$header['css']			= [''];
		$footer['javascripts']	= ['modules/crud.js?v1.0'];

		$this->load->view('includes/header', $header);
		$this->load->view('crud/index');
		$this->load->view('includes/footer', $footer);
	}

    public function get_crud() 
    {
        if($this->input->post()){
            $result['data'] = $this->Crud_model->get_crud();

            header('Content-Type: application/json');
            echo json_encode($result);
        }else{
            show_404();
        }
    }

    public function get_crud_by_id() 
    {
        if($this->input->post()){
            $result['data'] = $this->Crud_model->get_crud($id = $this->input->post('id'));

            header('Content-Type: application/json');
            echo json_encode($result);
        }else{
            show_404();
        }
    }

    public function get_pagination() 
    {
        if($this->input->post()){
            $result['data'] = $this->Crud_model->get_pagination($limit = $this->input->post('limit'), $search = $this->input->post('search'));

            header('Content-Type: application/json');
            echo json_encode($result);
        }else{
            show_404();
        }
    }

    public function load_table() 
    {
        if($this->input->post()){
            $result['data'] = $this->Crud_model->get_crud($id = 0, $limit = $this->input->post('limit'), $offset = $this->input->post('offset'), $search = $this->input->post('search'));

            header('Content-Type: application/json');
            echo json_encode($result);
        }else{
            show_404();
        }
    }

    public function save()
    {
        if($this->input->post()){
        	if($this->input->post('action_type') == "create"){
    			$result['data'] = $this->Crud_model->add($this->input->post());
        	}else if($this->input->post('action_type') == "update"){
    			$result['data'] = $this->Crud_model->update($this->input->post());
        	}else{
    			$result['data'] = $this->Crud_model->delete($this->input->post());
        	}
    		
    		header("Content-Type: application/json", true);
    		$this->output->set_output(print(json_encode($result)));
    		exit();
        }else{
            show_404();
        }
	}
}
