<!-- 


  /****************************************/
  /****************************************/
  /*            DEVELOPED BY:             */
  /*            KENN JEUS SAGUN           */
  /****************************************/
  /****************************************/

-->

<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Kenn Jeus Sagun - Sample CodeIgniter CRUD">
    <meta name="author" content="Kenn Jeus Sagun - jeussagun@gmail.com">
    <link rel="icon" href="<?= base_url();?>assets/img/sample_crud_ico.ico">

    <title><?= $nav?> - Sample Crud</title>
    
    <script src="<?= base_url();?>assets/js/popper.min.js"></script>
    <script src="<?= base_url();?>assets/js/jquery.min.js" type="text/javascript"></script>
    
    <link href="<?= base_url();?>assets/css/bootstrap.css" rel="stylesheet">
    <script src="<?= base_url();?>assets/js/bootstrap.min.js"></script>
    <link href="<?= base_url();?>assets/css/custom.css" rel="stylesheet">

    <link href="<?= base_url();?>assets/css/sweetalert2.min.css" rel="stylesheet">
    <script src="<?= base_url();?>assets/js/sweetalert2.min.js"></script>

    <link href="<?= base_url();?>assets/css/bootstrap-select.min.css" rel="stylesheet">
    <script src="<?= base_url();?>assets/js/bootstrap-select.min.js" type="text/javascript"></script>

    <?php if(isset($css) AND array_check($css)): ?>
      <?php foreach ($css as $css_value) : ?>
        <link href="<?= base_url() . "assets/css/" . $css_value?>" rel="stylesheet" type="text/css" />
      <?php endforeach;?>
    <?php endif;?>

    <script type="text/javascript">
      var base_url  = "<?= base_url();?>";
    </script>

  </head>

  <body>
    <nav class="navbar navbar-expand-md navbar-dark sticky-top bg-info">      
      <a class="navbar-brand" href="<?= base_url();?>"><img src="<?= base_url();?>assets/img/sample_crud_logo.png" alt="Sample CRUD Logo" class="img-fluid" alt="Responsive image" style="max-width: 150px;"></a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarCollapse">
      </div>
    </nav>

    <div class="modal fade" id="confirmModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header badge-info">
            <h5 class="modal-title" id="exampleModalLabel">Confirm</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
            <div class="modal-body">
              <div class="col-12">
                  <span class="confirm-message"></span>
              </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-sm btn-secondary" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-sm btn-primary btn-confirm-delete" id="confirm">Submit</button>
            </div>
        </div>
      </div>
    </div>

    <div class="container-fluid">
      <div class="row">