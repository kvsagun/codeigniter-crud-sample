<!-- 


  /****************************************/
  /****************************************/
  /*            DEVELOPED BY:             */
  /*            KENN JEUS SAGUN           */
  /****************************************/
  /****************************************/

-->

</div>
    </div>

    <!-- Icons -->
    <script src="<?= base_url();?>assets/js/feather.min.js"></script>
    <script src="<?= base_url();?>assets/js/modules/basic.js"></script>

    <?php if(isset($javascripts) AND array_check($javascripts)): ?>
      <?php foreach ($javascripts as $javascript) : ?>
        <?php if($javascript != ''):?>
          <script src="<?= base_url() . "assets/js/" . $javascript?>"></script>
        <?php endif;?>
      <?php endforeach;?>
    <?php endif;?>
    <script>
      feather.replace()
    </script>

  </body>
</html>
