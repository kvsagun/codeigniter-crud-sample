<?php

class Crud_model extends CI_MODEL{

	/****************************************/
	/****************************************/
	/*			 DEVELOPED BY:	 			*/
	/*			 KENN JEUS SAGUN 			*/
	/****************************************/
	/****************************************/


	function add($data)
	{
		$sql = "INSERT INTO employee(
					first_name, 
					last_name, 
					email, 
					address, 
					contact_number)
				VALUES(
				'".$data['first_name']."', 
				'".$data['last_name']."', 
				'".$data['email']."', 
				'".$data['address']."', 
				'".$data['contact_number']."')";

        $this->db->query($sql);
        return $this->db->insert_id();
	}

	function update($data)
	{
		$sql = "UPDATE employee
				SET first_name 			= '".$data['first_name']."',
					last_name 			= '".$data['last_name']."',
					email 				= '".$data['email']."',
					address 			= '".$data['address']."',
					contact_number 		= '".$data['contact_number']."',
					date_modified 		= NOW()
				WHERE id 				= '".$data['id']."'";

        $query = $this->db->query($sql);
		return $this->db->affected_rows();
	}

	function delete($data)
	{
		$sql = "UPDATE employee
				SET is_deleted = 1
				WHERE id = '".$data['id']."'";

        $query = $this->db->query($sql);
		return $this->db->affected_rows();
	}

	function get_pagination($limit = 10, $search = ''){

		$where_query = "";

		if($search !=''){
			$where_query .= " AND (
				    CONCAT(first_name, ' ', last_name) LIKE '%" . $search . "%'
				    OR
				    email LIKE '%" . $search . "%'
				    OR
				    address LIKE '%" . $search . "%'
				    OR
				    contact_number LIKE '%" . $search . "%'
					)";
		}

		 $sql = "SELECT 
					COUNT(*) AS total_count, 
					(
						COUNT(*) % " . $limit . "
					) AS remainder, 
					FLOOR(COUNT(*) / " . $limit . ") AS temp_num_pages
				FROM `employee`
				WHERE is_deleted = 0 " . $where_query;


        $result = $this->db->query($sql);
        return $result->result_array();
	}

	function get_crud($id = 0, $limit = 0, $offset = 0, $search = '')
	{
		$where_query = '';
		$limit_query = '';

		if($id != 0){
			$where_query .= " AND id = " . $id;
		}

		if($search !=''){
			$where_query .= " AND (
				    CONCAT(first_name, ' ', last_name) LIKE '%" . $search . "%'
				    OR
				    email LIKE '%" . $search . "%'
				    OR
				    address LIKE '%" . $search . "%'
				    OR
				    contact_number LIKE '%" . $search . "%'
					)";
		}

		if($limit != 0){
			$limit_query .= " LIMIT " . $offset . "," . $limit;
		}


		$sql = "SELECT
				  *
				FROM
				  employee
				WHERE is_deleted = 0 " . $where_query . "
				ORDER BY (
				    CASE
				      WHEN date_modified > date_created
				      THEN date_modified
				      ELSE date_created
				    END
				  ) DESC " . $limit_query;

        $result = $this->db->query($sql);
        return $result->result_array();
	}
}
?>