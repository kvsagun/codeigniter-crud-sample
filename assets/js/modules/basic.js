const Toast = Swal.mixin({
  toast: true,
  position: 'top-end',
  showConfirmButton: false,
  timer: 3000
});

var uiListContainter    = $('.table-list'),
  uiListTemplate      	= $('.table-list-template'),
  uiPageContainter    	= $('.pagination'),
  uiPageTemplate      	= $('.pagination-template'),
  btnConfirmDelete    	= $('.btn-confirm-delete'),
  btnNext             	= $('.btn-next'),
  btnPrevious         	= $('.btn-previous'),
  keyPress            	= null,
  oSelectedID         	= {};

var iOffset             = 0,
  iLimit              	= 10,
  wordSearch			      = "";

var pagination_url 		  = "";

btnNext.off('click').on('click', function(e) {
	e.preventDefault();

	iOffset 			+= iLimit;
	load_table(iOffset, iLimit);
	set_table_pagination(pagination_url);

});

btnPrevious.off('click').on('click', function(e) {
	e.preventDefault();

	iOffset 			-= iLimit;
	load_table(iOffset, iLimit);
	set_table_pagination(pagination_url);

});
      
$(".show-entries").change(function() {
	iLimit 				= $(".show-entries").val();
	iOffset 			= 0;
	load_table(iOffset, iLimit);
	set_table_pagination(pagination_url);
});

function set_table_pagination(url){

  var data = {
      limit 		: iLimit,
      search 		: wordSearch
  };

  $.ajax({
      type 			: 'POST',
      url 			: url,
      data 			: data,
      datatype 		: 'json',
      returnType 	: 'json',
      beforeSend	: function () {

        uiPageContainter.empty();

      },
      success 		: function(oData) {

        var data 					= oData.data;

        if(data.length > 0) {

            var total_pages			= (data[0].remainder != 0) ? (parseInt(data[0].temp_num_pages) + 1) : data[0].temp_num_pages;

            var current_page 		= (iOffset / iLimit) + 1;


            /* POPULATE PAGES BASED ON TOTAL PAGES*/

            if(total_pages 			<= 10){

              for(var i = 0; i < total_pages; i++){

                var page_num 		= i + 1;

                var uiPageClone 	= uiPageTemplate.clone();
                uiPageClone.removeClass('pagination-template');

                if(page_num 		== current_page) {
                  uiPageClone.addClass('active');
                }

                if(page_num 		== current_page) {
                  uiPageClone.addClass('active');
                }
                
                uiPageClone.find('.page-link').html(page_num);
                uiPageClone.find('.page-link').attr('offset', (page_num * iLimit));

                uiPageClone.show();
                uiPageContainter.append(uiPageClone); 

              }


            }else if((current_page <= (total_pages - 1)) && (current_page >= (total_pages - 8))){

                // first page

                var uiPageClone 	= uiPageTemplate.clone();
                uiPageClone.removeClass('pagination-template');

                if(current_page 	== 1) {
                  uiPageClone.addClass('active');
                }

                uiPageClone.find('.page-link').html('1');
                uiPageClone.find('.page-link').attr('offset', iLimit);

                uiPageClone.show();
                uiPageContainter.append(uiPageClone);   

                var last_page 		= total_pages - 1;

                var uiPageClone 	= uiPageTemplate.clone();
                uiPageClone.removeClass('pagination-template');
                uiPageClone.addClass('disabled'); 
                uiPageClone.find('.page-link').html('<span>...</span>');
                uiPageClone.show();
                uiPageContainter.append(uiPageClone); 

                for(var i = (total_pages - 10); i < (total_pages - 1); i++){

                  var page_num 		= i + 1;

                  var uiPageClone 	= uiPageTemplate.clone();
                  uiPageClone.removeClass('pagination-template');

                  if(page_num 		== current_page) {
                    uiPageClone.addClass('active');
                  }

                  if(page_num 		== current_page) {
                    uiPageClone.addClass('active');
                  }
                  
                  uiPageClone.find('.page-link').html(page_num);
                  uiPageClone.find('.page-link').attr('offset', (page_num * iLimit));

                  uiPageClone.show();
                  uiPageContainter.append(uiPageClone); 

                }

              }else{

              // first page

              var uiPageClone 		= uiPageTemplate.clone();
              uiPageClone.removeClass('pagination-template');

              if(current_page 		== 1) {
                uiPageClone.addClass('active');
              }

              uiPageClone.find('.page-link').html('1');
              uiPageClone.find('.page-link').attr('offset', iLimit);

              uiPageClone.show();
              uiPageContainter.append(uiPageClone);   


              if(current_page > 8){

                var uiPageClone 	= uiPageTemplate.clone();
                uiPageClone.removeClass('pagination-template');
                uiPageClone.addClass('disabled'); 
                uiPageClone.find('.page-link').html('<span>...</span>');
                uiPageClone.show();
                uiPageContainter.append(uiPageClone); 

                for(var i = (current_page-5); i < (current_page + 4); i++){
                  var page_num 		= i + 1;

                  var uiPageClone 	= uiPageTemplate.clone();
                  uiPageClone.removeClass('pagination-template');

                  if(page_num 		== current_page) {
                    uiPageClone.addClass('active');
                  }
                  
                  uiPageClone.find('.page-link').html(page_num);
                  uiPageClone.find('.page-link').attr('offset', (page_num * iLimit));

                  uiPageClone.show();
                  uiPageContainter.append(uiPageClone); 
                }

                var uiPageClone 	= uiPageTemplate.clone();
                uiPageClone.removeClass('pagination-template');
                uiPageClone.addClass('disabled'); 
                uiPageClone.find('.page-link').html('<span>...</span>');
                uiPageClone.show();
                uiPageContainter.append(uiPageClone); 


              }else{

                for(var i = 1; i < 9; i++){

                  var page_num 		= i + 1;

                  var uiPageClone 	= uiPageTemplate.clone();
                  uiPageClone.removeClass('pagination-template');

                  if(page_num 		== current_page) {
                    uiPageClone.addClass('active');
                  }
                  
                  uiPageClone.find('.page-link').html(page_num);
                  uiPageClone.find('.page-link').attr('offset', (page_num * iLimit));

                  uiPageClone.show();
                  uiPageContainter.append(uiPageClone); 

                }

                var uiPageClone 	= uiPageTemplate.clone();
                uiPageClone.removeClass('pagination-template');
                uiPageClone.addClass('disabled'); 
                uiPageClone.find('.page-link').html('<span>...</span>');
                uiPageClone.show();
                uiPageContainter.append(uiPageClone); 

              }


              var last_page 		= total_pages - 1;

              // last page

              var uiPageClone 		= uiPageTemplate.clone();
              uiPageClone.removeClass('pagination-template');

              if(current_page 		== last_page) {
                uiPageClone.addClass('active');
              }
              
              uiPageClone.find('.page-link').html(last_page);
              uiPageClone.find('.page-link').attr('offset', (last_page * iLimit));

              uiPageClone.show();
              uiPageContainter.append(uiPageClone); 

            }

            $('.page-link').off('click').on('click', function(e) {
              e.preventDefault();

              iOffset 				= (parseInt($(this).html()) - 1) * iLimit;

              load_table(iOffset, iLimit);
              set_table_pagination(pagination_url);

            });

        }

      }
  });
}

$('.search').keyup(function () {
  clearInterval(keyPress);
  keyPress 			= setInterval(function (){
    var len 		= $('.search').val().length;
    if (len 		!= 0){
      wordSearch 	= $('.search').val();
      load_table(iOffset, iLimit, wordSearch);
  	}else{
      load_table(iOffset, iLimit);
      $('.btn-previous').show();
      $('.btn-next').show();
    }
    set_table_pagination(pagination_url);
    clearInterval(keyPress);
  }, 1000);

});